//
//  FavoritesTableViewCell.swift
//  MangaApp
//
//  Created by Vasil Smilenov on 1/10/22.
//

import UIKit

class FavoritesTableViewCell: UITableViewCell {

    @IBOutlet weak var favoritesImageView: UIImageView!
    @IBOutlet weak var favoritesTitleLabel: UILabel!
    @IBOutlet weak var favoritesTypeLabel: UILabel!
    @IBOutlet weak var favoritesScoreLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
