//
//  MangaListTableViewController.swift
//  MangaListTableViewController
//
//  Created by Vasil Smilenov on 8/21/21.
//

import UIKit
import Alamofire
import SDWebImage
class MangaListTableViewController: UITableViewController {
    
    var tops = [MangaRequestStruct]()
    var loadPage = 1
    let maxPage = 20
    var currentLoadPage = 1
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadMangaList(page: loadPage)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
   
    
    
    func loadMangaList(page: Int) {
        activityIndicator.startAnimating()
        MangaService.shared.loadMangaList(page: page) { result, errorMessage, data in
            self.activityIndicator.stopAnimating()
            self.activityIndicator.hidesWhenStopped = true
            if (result) {
                if let data = data {
                    self.tops.append(contentsOf: data )
                    self.tableView.reloadData()
                } else {
                    print(errorMessage)
                }
            }
        }
    }
   
   
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //print("Will display - \(indexPath.row)")
        let lastItem = tops.count - 1
        if indexPath.row == lastItem {
            if currentLoadPage < maxPage {
                currentLoadPage += 1
                self.loadMangaList(page: self.currentLoadPage)
            }
        }
    }
   
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tops.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mangaCell", for: indexPath) as! MangaTableViewCell
        let top = self.tops[indexPath.row]
        let imageURL = URL(string: top.image_url)!
        cell.mangaImageView.sd_setImage(with: imageURL)
        cell.mangaTitleLabel.text = "Title: \(top.title)"
        cell.mangaTypeLabel.text = "Type: \(top.type)"
        cell.mangaRankLabel.text = "Rank: \(top.rank)"
        return cell
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMangaDetails" {
            let dvc = segue.destination as! MangaDetailsViewController
            if let indexPath = tableView.indexPathForSelectedRow {
                //print(indexPath.row)
                dvc.id = self.tops[indexPath.row].mal_id
                dvc.backButtonTitle = "Manga"
            }
        }
    }
    

}
