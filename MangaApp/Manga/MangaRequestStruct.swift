//
//  MangaStruct.swift
//  MangaStruct
//
//  Created by Vasil Smilenov on 8/19/21.
//

import Foundation

struct MangaRequestStruct: Codable {
    var mal_id: Int
    var rank: Int
    var title: String
    var type: String
    var start_date: String?
    var end_date: String?
    var members: Int
    var score: Double
    var image_url: String
}
