//
//  File.swift
//  
//
//  Created by Vasil Smilenov on 11/26/21.
//

import Foundation

struct PicturesRequestStruct: Codable {
    var pictures: [Pictures]
}

struct Pictures: Codable {
    var large: String
    var small: String
}
