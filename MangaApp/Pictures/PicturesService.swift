//
//  PicturesService.swift
//  MangaApp
//
//  Created by Vasil Smilenov on 12/14/21.
//

import Foundation
import Alamofire

class PicturesService {
    static let shared = PicturesService()
    func loadPictures(id: Int, loadType: String, completionHandler: @escaping (Bool, String, [Pictures]?) -> ()) {
        let request = AF.request("https://api.jikan.moe/v3/\(loadType)/\(id)/pictures")
        request.responseDecodable(of: PicturesRequestStruct.self) { (response) in
            switch(response.result) {
            case .success(let pictureRequest):
                completionHandler(true, "Success", pictureRequest.pictures)
            case .failure(let error):
                completionHandler(false, error.errorDescription ?? "Error", nil)
            }
        }
    }

    
    private init() {}
}
