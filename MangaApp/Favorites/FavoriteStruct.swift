//
//  FavoriteStruct.swift
//  MangaApp
//
//  Created by Vasil Smilenov on 1/12/22.
//

import Foundation

struct FavoriteStruct: Codable {
    var mal_id: Int
    var title: String
    var image_url: String
    var score: Double
    var type: String
    
    init(mal_id: Int, title: String, image_url: String, score: Double, type: String) {
        self.mal_id = mal_id
        self.title = title
        self.image_url = image_url
        self.score = score
        self.type = type
    }
        
}
