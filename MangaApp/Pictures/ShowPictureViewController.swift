//
//  ShowPictureViewController.swift
//  MangaApp
//
//  Created by Vasil Smilenov on 11/30/21.
//

import UIKit
import Alamofire
import SDWebImage

class ShowPictureViewController: UIViewController {
    
    var imageUrl = ""
    @IBOutlet weak var mangaAnimeImageView: UIImageView!
    
    
   
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let imageURL = URL(string: imageUrl)!
        mangaAnimeImageView.sd_setImage(with: imageURL)
    }
    

    

}
