//
//  MangaDetailsRequestStruct.swift
//  MangaDetailsRequestStruct
//
//  Created by Vasil Smilenov on 8/19/21.
//

import Foundation

struct MangaDetailsRequestStruct: Codable {
    var mal_id: Int
    var title: String
    var title_japanese: String
    var status: String
    var image_url: String
    var type: String
    var chapters: Int?
    var publishing: Bool
    var published: PublishedStruct
    var score: Double
    var scored_by: Int
    var members: Int
    var favorites: Int
    var synopsis: String
}

struct PublishedStruct: Codable {
    var from: String?
    var to: String?
}
