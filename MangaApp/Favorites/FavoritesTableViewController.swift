//
//  FavoritesTableViewController.swift
//  MangaApp
//
//  Created by Vasil Smilenov on 1/10/22.
//

import UIKit
import SDWebImage

class FavoritesTableViewController: UITableViewController {
    
    var favoritesList = [FavoriteStruct]()
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadSavedFavoritesListAndReloadTable()
        print("View will appear")
    }
    
    func loadSavedFavoritesListAndReloadTable() {
        do {
            // IMPORTANT!!!! Class should inherit Codable protocol
            // get data from UserDefaults
            if let encodedData = UserDefaults.standard.object(forKey: "keyFavoritesList") as? Data {
                // try to decode the data
                favoritesList = try PropertyListDecoder().decode([FavoriteStruct].self, from: encodedData)
                // reload the table
                tableView.reloadData()
            }
        }
        catch {
            print("Unexpected error while decoding: \(error)")
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return favoritesList.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "favoritesCell", for: indexPath) as! FavoritesTableViewCell
        let favorite = favoritesList[indexPath.row]
        cell.favoritesTitleLabel.text = "Title: \(favorite.title)"
        cell.favoritesTypeLabel.text = "Type: \(favorite.type)"
        cell.favoritesScoreLabel.text = "Score: \(favorite.score)"
        let imageURL = URL(string: favorite.image_url)!
        cell.favoritesImageView.sd_setImage(with: imageURL)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let favoritesListItem = favoritesList[indexPath.row]
        print("\(favoritesListItem.title)")
        print("\(favoritesListItem.type)")
        if favoritesListItem.type == "Manga" {
            performSegue(withIdentifier: "showMangaDetails", sender: nil)
        } else {
            performSegue(withIdentifier: "showAnimeDetails", sender: nil)
        }
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            favoritesList.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .middle)
            do {
                let archivedFavoritesList = try PropertyListEncoder().encode(favoritesList)
                UserDefaults.standard.set(archivedFavoritesList, forKey: "keyFavoritesList")
            } catch {
                print("Unexpected error while encoding: \(error)")
            }
        }
    }
    override func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Remove"
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showAnimeDetails" {
            let dvc = segue.destination as! AnimeDetailsViewController
            if let indexPath = tableView.indexPathForSelectedRow {
                print(indexPath.row)
                dvc.id = self.favoritesList[indexPath.row].mal_id
                dvc.backButtonTitle = "Favorites"
            }
        }
         if segue.identifier == "showMangaDetails" {
            let secondDvc = segue.destination as! MangaDetailsViewController
            if let indexPath = tableView.indexPathForSelectedRow {
                secondDvc.id = self.favoritesList[indexPath.row].mal_id
                secondDvc.backButtonTitle = "Favorites"
            }
        }
    }
    
   


  

}
