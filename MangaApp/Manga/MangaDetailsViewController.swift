//
//  ViewController.swift
//  MangaApp
//
//  Created by Vasil Smilenov on 8/19/21.
//

import UIKit
import Alamofire
import SDWebImage

class MangaDetailsViewController: UIViewController {
    @IBOutlet weak var mangaDetailsImageView: UIImageView!
    @IBOutlet weak var englishTitleLabel: UILabel!
    @IBOutlet weak var japaneseTitleLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var chaptersLabel: UILabel!
    @IBOutlet weak var publishedFromLabel: UILabel!
    @IBOutlet weak var publishedToLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var scoredByLabel: UILabel!
    @IBOutlet weak var mambersLabel: UILabel!
    @IBOutlet weak var favoritesLabel: UILabel!
    @IBOutlet weak var mangaTextView: UITextView!
    @IBOutlet weak var mangaActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var mangaGalleryButton: UIButton!
    var id = Int()
    let backButton = UIBarButtonItem()
    var favoritesList = [FavoriteStruct]()
    var mangaDetails: MangaDetailsRequestStruct? = nil
    let keySavedFavoritesList = "keyFavoritesList"
    var backButtonTitle = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadMangaDetails(id: id)
        mangaActivityIndicator.hidesWhenStopped = true
        mangaDetailsImageView.isHidden = true
        englishTitleLabel.isHidden = true
        japaneseTitleLabel.isHidden = true
        typeLabel.isHidden = true
        statusLabel.isHidden = true
        statusLabel.isHidden = true
        chaptersLabel.isHidden = true
        publishedFromLabel.isHidden = true
        publishedToLabel.isHidden = true
        scoreLabel.isHidden = true
        scoredByLabel.isHidden = true
        mambersLabel.isHidden = true
        favoritesLabel.isHidden = true
        mangaTextView.isHidden = true
        mangaGalleryButton.isHidden = true
        self.backButton.title =  backButtonTitle
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMangaPictures" {
            let dvc = segue.destination as! PicturesViewController
            dvc.id = id
            dvc.loadType = "manga"
        }
    }
    
    
    @IBAction func onSaveButtonClicked(_ sender: Any) {
        if let mangaDetails = mangaDetails {
           
            saveFavoritesList(mangaDetails:mangaDetails)
        }
    }
    
    func saveFavoritesList(mangaDetails: MangaDetailsRequestStruct) {
        do {
            if let encodedData = UserDefaults.standard.object(forKey: "keyFavoritesList") as? Data {
                // try to decode the data
                favoritesList = try PropertyListDecoder().decode([FavoriteStruct].self, from: encodedData)
                for favorite in favoritesList {
                    if favorite.mal_id == mangaDetails.mal_id {
                        return
                    }
                }
                favoritesList.insert(FavoriteStruct.init(mal_id: mangaDetails.mal_id, title: mangaDetails.title, image_url: mangaDetails.image_url, score: mangaDetails.score, type: "Manga"), at: 0)
            }
            let archivedFavoritesList = try PropertyListEncoder().encode(favoritesList)
            UserDefaults.standard.set(archivedFavoritesList, forKey: keySavedFavoritesList)
        } catch {
            print("Unexpected error while encoding: \(error)")
        }
    }

    func loadMangaDetails(id: Int) {
        mangaActivityIndicator.startAnimating()
        MangaService.shared.loadMangaDetails(id: id) { result, errorMessage, data in
            if (result) {
                if let data = data {
                    self.mangaDetails = data
                    self.mangaActivityIndicator.stopAnimating()
                    self.mangaDetailsImageView.isHidden = false
                    self.englishTitleLabel.isHidden = false
                    self.japaneseTitleLabel.isHidden = false
                    self.typeLabel.isHidden = false
                    self.statusLabel.isHidden = false
                    self.statusLabel.isHidden = false
                    self.chaptersLabel.isHidden = false
                    self.publishedFromLabel.isHidden = false
                    self.publishedToLabel.isHidden = false
                    self.scoreLabel.isHidden = false
                    self.scoredByLabel.isHidden = false
                    self.mambersLabel.isHidden = false
                    self.favoritesLabel.isHidden = false
                    self.mangaTextView.isHidden = false
                    self.mangaGalleryButton.isHidden = false
                    let imageURL = URL(string: data.image_url)!
                    self.mangaDetailsImageView.sd_setImage(with: imageURL)
                    self.englishTitleLabel.text = "English Title: \(data.title)"
                    self.japaneseTitleLabel.text = "Japanese Title: \(data.title_japanese)"
                    self.typeLabel.text = "Type: \(data.type)"
                    self.statusLabel.text = "Status: \(data.status)"
                    if let chapters = data.chapters {
                        self.chaptersLabel.text = "Chapters: \(chapters)"
                    } else {
                        self.chaptersLabel.text = "Chapters: No Info"
                    }
                    if let publishedFrom = data.published.from {
                        let publishedFromEndIndex = publishedFrom.index(publishedFrom.startIndex, offsetBy: 10)
                        let publishedFromRange = publishedFrom.startIndex..<publishedFromEndIndex
                        self.publishedFromLabel.text = "From: \(publishedFrom[publishedFromRange])"
                    } else {
                        self.publishedFromLabel.text = "From: No Info"
                    }
                    if let publishedTo = data.published.to {
                        let publishedToEndIndex = publishedTo.index(publishedTo.startIndex, offsetBy: 10)
                        let publishedToRange = publishedTo.startIndex..<publishedToEndIndex
                        self.publishedToLabel.text = "To: \(publishedTo[publishedToRange])"
                    } else {
                        self.publishedToLabel.text = "To: No Info"
                    }
                    self.scoreLabel.text = "Score: \(data.score)"
                    self.scoredByLabel.text = "Scored by: \(data.scored_by)"
                    self.mambersLabel.text = "Members: \(data.members)"
                    self.favoritesLabel.text = "Favorites: \(data.favorites)"
                    self.mangaTextView.text = data.synopsis
                } else {
                    print(errorMessage)
                }
            }
        }
    }
}

