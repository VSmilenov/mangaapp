//
//  TopAnimeRequestStruct.swift
//  MangaApp
//
//  Created by Vasil Smilenov on 11/4/21.
//

import Foundation

struct TopAnimeRequestStruct: Codable {
    var top: [AnimeRequestStruct]
}
