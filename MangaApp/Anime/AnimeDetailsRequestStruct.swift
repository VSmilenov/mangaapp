//
//  AnimeDetailsRequestStruct.swift
//  MangaApp
//
//  Created by Vasil Smilenov on 11/4/21.
//

import Foundation

struct AnimeDetailsRequestStruct: Codable {
    var mal_id: Int
    var title: String
    var title_japanese: String
    var status: String
    var image_url: String
    var type: String
    var episodes: Int?
    var airing: Bool
    var aired: AnimePublishedStruct
    var score: Double
    var scored_by: Int
    var members: Int
    var favorites: Int
    var synopsis: String
}

struct AnimePublishedStruct: Codable {
    var from: String?
    var to: String?
}
