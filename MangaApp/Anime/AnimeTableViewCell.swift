//
//  AnimeTableViewCell.swift
//  MangaApp
//
//  Created by Vasil Smilenov on 11/4/21.
//

import UIKit

class AnimeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var animeImageView: UIImageView!
    @IBOutlet weak var animeTitleLabel: UILabel!
    @IBOutlet weak var animeRankLabel: UILabel!
    @IBOutlet weak var animeTypeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }

}
