//
//  AnimeService.swift
//  MangaApp
//
//  Created by Vasil Smilenov on 12/14/21.
//

import Foundation
import Alamofire



class AnimeService {
    static let shared = AnimeService()
    func loadAnimeList(page: Int, completionHandler: @escaping (Bool, String, [AnimeRequestStruct]?) -> ()) {
        let request = AF.request("https://api.jikan.moe/v3/top/anime/\(page)")
        request.responseDecodable(of: TopAnimeRequestStruct.self) { (response) in
            switch(response.result) {
            case .success(let topRequest):
                completionHandler(true, "Success", topRequest.top)
            case .failure(let error):
                completionHandler(false, error.errorDescription ?? "Error", nil)
            }
        }
    }
    func loadAnimeDetails(id: Int, completionHandler: @escaping (Bool, String, AnimeDetailsRequestStruct?) -> ()) {
        let request = AF.request("https://api.jikan.moe/v3/anime/\(id)")
        request.responseDecodable(of: AnimeDetailsRequestStruct.self) { (response) in
            switch(response.result) {
            case .success(let detailsRequest):
                completionHandler(true, "Success", detailsRequest)
            case .failure(let error):
                completionHandler(false, error.errorDescription ?? "Error", nil)
            }
        }
    }
    
    
    private init() {}
}
