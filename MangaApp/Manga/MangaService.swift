//
//  MangaService.swift
//  MangaApp
//
//  Created by Vasil Smilenov on 12/10/21.
//

import Foundation
import Alamofire

class MangaService {
    static let shared = MangaService()
    func loadMangaList(page: Int, completionHandler: @escaping (Bool, String, [MangaRequestStruct]?) -> ()) {
        let request = AF.request("https://api.jikan.moe/v3/top/manga/\(page)")
        request.responseDecodable(of: TopRequestStruct.self) { (response) in
            switch(response.result) {
            case .success(let topRequest):
                completionHandler(true, "Success", topRequest.top)
            case .failure(let error):
                completionHandler(false, error.errorDescription ?? "Error", nil)
            }
        }
    }
    
    func loadMangaDetails(id: Int, completionHandler: @escaping (Bool, String, MangaDetailsRequestStruct?) -> ()) {
        let request = AF.request("https://api.jikan.moe/v3/manga/\(id)")
        request.responseDecodable(of: MangaDetailsRequestStruct.self) { (response) in
            switch(response.result) {
            case .success(let detailsRequest):
                completionHandler(true, "Success", detailsRequest)
            case .failure(let error):
                completionHandler(false, error.errorDescription ?? "Error", nil)
            }
        }
    }

    
    
    private init() {}
}
