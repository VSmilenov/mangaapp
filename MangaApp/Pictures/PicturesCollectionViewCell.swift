//
//  PicturesCollectionViewCell.swift
//  MangaApp
//
//  Created by Vasil Smilenov on 11/26/21.
//

import UIKit

class PicturesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var animeMangaImageView: UIImageView!
}
