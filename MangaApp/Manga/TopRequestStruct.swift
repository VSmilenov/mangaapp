//
//  RequestTopStruct.swift
//  RequestTopStruct
//
//  Created by Vasil Smilenov on 8/19/21.
//

import Foundation

struct TopRequestStruct: Codable {
    var top: [MangaRequestStruct]
}
