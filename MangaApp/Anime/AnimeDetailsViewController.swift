//
//  AnimeDetailsViewController.swift
//  MangaApp
//
//  Created by Vasil Smilenov on 11/4/21.
//

import UIKit
import Alamofire
import SDWebImage

class AnimeDetailsViewController: UIViewController {
    @IBOutlet weak var animeEglishTitleLabel: UILabel!
    @IBOutlet weak var animeJapaneseTitleLabel: UILabel!
    @IBOutlet weak var animeImageView: UIImageView!
    @IBOutlet weak var animeTypeLabel: UILabel!
    @IBOutlet weak var animeChaptersLabel: UILabel!
    @IBOutlet weak var animePublishedFromLabel: UILabel!
    @IBOutlet weak var animePublishedToLabel: UILabel!
    @IBOutlet weak var animeScoreLabel: UILabel!
    @IBOutlet weak var animeScoredByLabel: UILabel!
    @IBOutlet weak var animeMambersLabel: UILabel!
    @IBOutlet weak var animeFavoritesLabel: UILabel!
    @IBOutlet weak var animeStatusLabel: UILabel!
    @IBOutlet weak var animeTextView: UITextView!
    @IBOutlet weak var animeActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var animeGalleryButton: UIButton!
    
    
    var id = Int()
    let backButton = UIBarButtonItem()
    var favoritesList = [FavoriteStruct]()
    var animeDetails: AnimeDetailsRequestStruct? = nil
    let keySavedFavoritesList = "keyFavoritesList"
    var backButtonTitle = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        loadAnimeDetails(id: id)
        animeActivityIndicator.hidesWhenStopped = true
        self.animeEglishTitleLabel.isHidden = true
        self.animeJapaneseTitleLabel.isHidden = true
        self.animeTextView.isHidden = true
        self.animeImageView.isHidden = true
        self.animeTypeLabel.isHidden = true
        self.animeScoreLabel.isHidden = true
        self.animeStatusLabel.isHidden = true
        self.animeMambersLabel.isHidden = true
        self.animeChaptersLabel.isHidden = true
        self.animeFavoritesLabel.isHidden = true
        self.animeScoredByLabel.isHidden = true
        self.animePublishedToLabel.isHidden = true
        self.animePublishedFromLabel.isHidden = true
        self.animeGalleryButton.isHidden = true
        self.backButton.title = backButtonTitle
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showAnimePictures" {
            let dvc = segue.destination as! PicturesViewController
            dvc.id = id
            dvc.loadType = "anime"
        }
    }
    
    @IBAction func onSaveButtonClicked(_ sender: Any) {
        if let animeDetails = animeDetails {
           
            saveFavoritesList(animeDetails:animeDetails)
        }
    }
    
    func saveFavoritesList(animeDetails: AnimeDetailsRequestStruct) {
        do {
            if let encodedData = UserDefaults.standard.object(forKey: keySavedFavoritesList) as? Data {
                // try to decode the data
                favoritesList = try PropertyListDecoder().decode([FavoriteStruct].self, from: encodedData)
                for favorite in favoritesList {
                    if favorite.mal_id == animeDetails.mal_id {
                        return
                    }
                }
                favoritesList.insert(FavoriteStruct.init(mal_id: animeDetails.mal_id, title: animeDetails.title, image_url: animeDetails.image_url, score: animeDetails.score, type: "Anime"), at: 0)
            }
            let archivedFavoritesList = try PropertyListEncoder().encode(favoritesList)
            UserDefaults.standard.set(archivedFavoritesList, forKey: keySavedFavoritesList)
        } catch {
            print("Unexpected error while encoding: \(error)")
        }
    }
    
    
    func loadAnimeDetails(id: Int) {
        animeActivityIndicator.startAnimating()
        AnimeService.shared.loadAnimeDetails(id: id) { result, errorMessage, data in
            if (result) {
                if let data = data {
                    self.animeDetails = data
                    self.animeActivityIndicator.stopAnimating()
                    self.animeEglishTitleLabel.isHidden = false
                    self.animeJapaneseTitleLabel.isHidden = false
                    self.animeTextView.isHidden = false
                    self.animeImageView.isHidden = false
                    self.animeTypeLabel.isHidden = false
                    self.animeScoreLabel.isHidden = false
                    self.animeStatusLabel.isHidden = false
                    self.animeMambersLabel.isHidden = false
                    self.animeChaptersLabel.isHidden = false
                    self.animeFavoritesLabel.isHidden = false
                    self.animeScoredByLabel.isHidden = false
                    self.animePublishedToLabel.isHidden = false
                    self.animePublishedFromLabel.isHidden = false
                    self.animeGalleryButton.isHidden = false
                    let imageURL = URL(string: data.image_url)!
                    self.animeImageView.sd_setImage(with: imageURL)
                    self.animeEglishTitleLabel.text = "English Title: \(data.title)"
                    self.animeJapaneseTitleLabel.text = "Japanese Title: \(data.title_japanese)"
                    self.animeTypeLabel.text = "Type: \(data.type)"
                    self.animeStatusLabel.text = "Status: \(data.status)"
                    if let episodes = data.episodes {
                        self.animeChaptersLabel.text = "Episodes: \(episodes)"
                    } else {
                        self.animeChaptersLabel.text = "Episodes: No Info"
                    }
                    if let publishedFrom = data.aired.from {
                        let publishedFromEndIndex = publishedFrom.index(publishedFrom.startIndex, offsetBy: 10)
                        let publishedFromRange = publishedFrom.startIndex..<publishedFromEndIndex
                        self.animePublishedFromLabel.text = "From: \(publishedFrom[publishedFromRange])"
                    } else {
                        self.animePublishedFromLabel.text = "From: No Info"
                    }
                    if let publishedTo = data.aired.to {
                        let publishedToEndIndex = publishedTo.index(publishedTo.startIndex, offsetBy: 10)
                        let publishedToRange = publishedTo.startIndex..<publishedToEndIndex
                        self.animePublishedToLabel.text = "To: \(publishedTo[publishedToRange])"
                    } else {
                        self.animePublishedToLabel.text = "To: No Info"
                    }
                    self.animeScoreLabel.text = "Score: \(data.score)"
                    self.animeScoredByLabel.text = "Scored by: \(data.scored_by)"
                    self.animeMambersLabel.text = "Members: \(data.members)"
                    self.animeFavoritesLabel.text = "Favorites: \(data.favorites)"
                    self.animeTextView.text = data.synopsis
                }
            }
        }
    }

    

}
