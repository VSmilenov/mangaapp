//
//  PicturesViewController.swift
//  MangaApp
//
//  Created by Vasil Smilenov on 11/29/21.
//

import UIKit
import Alamofire
import SDWebImage


class PicturesViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    
    var pictures = [Pictures]()
    var id = Int()
    var loadType = ""
    
   
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var statusLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadPictures(id: id)
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    
    func loadPictures(id: Int) {
        activityIndicator.startAnimating()
        PicturesService.shared.loadPictures(id: id, loadType: loadType) { result, errorMessage, data in
            self.activityIndicator.stopAnimating()
            self.activityIndicator.hidesWhenStopped = true
            if (result) {
                if let data = data {
                    self.pictures = data
                    self.collectionView.reloadData()
                } else {
                    self.statusLabel.text = "There are no pictures."
                    print(errorMessage)
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pictures.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "pictureCell", for: indexPath) as! PicturesCollectionViewCell
        let picture = pictures[indexPath.row]
        let pictureUrl = URL(string: picture.large)!
        cell.animeMangaImageView.sd_setImage(with: pictureUrl)
    
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/3.0
        let yourHeight = yourWidth

        return CGSize(width: yourWidth, height: yourHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMangaAnimePicture" {
            let dvc = segue.destination as! ShowPictureViewController
            if let indexPathList = collectionView.indexPathsForSelectedItems {
                dvc.imageUrl = pictures[indexPathList[0].row].large
            }
        }
    }
    
  
    

    
    

    

}
