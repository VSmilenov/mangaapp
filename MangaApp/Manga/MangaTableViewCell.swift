//
//  MangaTableViewCell.swift
//  MangaTableViewCell
//
//  Created by Vasil Smilenov on 8/21/21.
//

import UIKit

class MangaTableViewCell: UITableViewCell {
    @IBOutlet weak var mangaImageView: UIImageView!
    @IBOutlet weak var mangaTitleLabel: UILabel!
    @IBOutlet weak var mangaTypeLabel: UILabel!
    @IBOutlet weak var mangaRankLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

   
    }

}
