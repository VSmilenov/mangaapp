//
//  AnimeListTableViewController.swift
//  MangaApp
//
//  Created by Vasil Smilenov on 11/4/21.
//

import UIKit
import Alamofire
import SDWebImage

class AnimeListTableViewController: UITableViewController {
    
    var topAnime = [AnimeRequestStruct]()
    var currentPage = 1
    let maxPage = 20
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadAnimeList(page: 1)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func loadAnimeList(page: Int) {
        activityIndicator.startAnimating()
        AnimeService.shared.loadAnimeList(page: page) { result, errorMessage, data in
            self.activityIndicator.stopAnimating()
            self.activityIndicator.hidesWhenStopped = true
            if (result) {
                if let data = data {
                    self.topAnime.append(contentsOf: data )
                    self.tableView.reloadData()
                } else {
                    print(errorMessage)
                }
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastItem = topAnime.count - 1
        if indexPath.row == lastItem {
            if currentPage < maxPage {
                currentPage += 1
                self.loadAnimeList(page: self.currentPage)
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
    
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return topAnime.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "animeCell", for: indexPath) as! AnimeTableViewCell
        let top = self.topAnime[indexPath.row]
        let imageURL = URL(string: top.image_url)!
        cell.animeImageView.sd_setImage(with: imageURL)
        cell.animeTitleLabel.text = "Title: \(top.title)"
        cell.animeTypeLabel.text = "Type: \(top.type)"
        cell.animeRankLabel.text = "Rank: \(top.rank)"
        return cell
    }


    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showAnimeDetails" {
            let dvc = segue.destination as! AnimeDetailsViewController
            if let indexPath = tableView.indexPathForSelectedRow {
                print(indexPath.row)
                dvc.id = self.topAnime[indexPath.row].mal_id
                dvc.backButtonTitle = "Anime"
            }
        }
    }


}
